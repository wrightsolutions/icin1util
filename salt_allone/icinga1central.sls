icinga_central:
  # Install Icinga central server
  pkg.installed:
    - pkgs:
      - icinga-core
      - icinga-common
      - icinga-cgi
      - nagios-images
# Next nrpe-server package not required for central server
# Unless it is required to be externally monitored using incoming nrpe
      #- nagios-nrpe-server
# Above nrpe-server should be version 3.2 or newer or ensure appropriate cfg flag use
      - nagios-nrpe-plugin
# Above provides the executable /usr/lib/nagios/plugins/check_nrpe
      - monitoring-plugins-common
      - monitoring-plugins-basic
      - monitoring-plugins-standard
      # - monitoring-plugins
# Enable these last 3 packages if you want mysql/maria backed monitoring
      # - icinga-idoutils
      # - libdbd-mysql
      # - default-mysql-client
    - install_recommends: false


icinga_central_ilo:
  # Install Icinga central ilo depedencies
  # No need for libwww-perl libwww-robotrules-perl so install_recommends: false
  cmd.run:
    - name: >
       apt-get -y --no-install-recommends install
       libmonitoring-plugin-perl libio-socket-ssl-perl libxml-simple-perl
    # - install_recommends: false


icinga_nrpe:
  # Install Icinga nrpe remote client
  pkg.installed:
    - pkgs:
      - nagios-nrpe-server
      # - monitoring-plugins
      # - monitoring-plugins-basic
      - monitoring-plugins-common
      - monitoring-plugins-basic
      - monitoring-plugins-standard
      # - monitoring-plugins
    - install_recommends: false

# nagios    ALL=(ALL) NOPASSWD: /usr/lib/nagios/plugins/,/usr/bin/arrayprobe

